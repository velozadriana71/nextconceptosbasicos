// our-domain.com/news
import Link from 'next/link';
function NewsPage() {
  return (
    <>
      <h1>tHE NEWS PAGE</h1>
      <ul>
        <li>
          <Link href="/news/nextjs">NextJS is a great framework </Link>
        </li>
        <li>Something else</li>
      </ul>
    </>
  );
}
export default NewsPage;
